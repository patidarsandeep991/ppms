package com.aartek.prestigepoint.repositoryImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.aartek.prestigepoint.model.Enquiry;
import com.aartek.prestigepoint.repository.EnquiryRepository;

@Repository
public class EnquiryRepositoryImpl implements EnquiryRepository {
  @Autowired
  private HibernateTemplate hibernateTemplate;

  public void addEnquiryMessage(Enquiry enquiry) {
    hibernateTemplate.save(enquiry);
  }
}
