package com.aartek.prestigepoint.repository;

import com.aartek.prestigepoint.model.Enquiry;

public interface EnquiryRepository {

  public void addEnquiryMessage(Enquiry enquiry);

}
