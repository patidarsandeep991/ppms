package com.aartek.prestigepoint.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.aartek.prestigepoint.model.PhotoInFooter;
import com.aartek.prestigepoint.model.Registration;
import com.aartek.prestigepoint.model.Subject;
import com.aartek.prestigepoint.service.QuestionAnswerService;
import com.aartek.prestigepoint.service.StudentRegistrationService;
import com.aartek.prestigepoint.service.FooterPhotoService;
@Controller
public class LoginController {

	@Autowired
	private FooterPhotoService footerPhotoService ;
	
	@Autowired
	private StudentRegistrationService stuRegService;

	@Autowired
	private QuestionAnswerService questionAnswerService;

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String welcome(Map<String, Object> map, Model model) {
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiii");
		List<Subject> subjects = null;
		subjects = questionAnswerService.getAllSubjectName();
		/*List<PhotoInFooter> allStudentDetail=footerPhotoService.getAllStudentName();
		System.out.println(allStudentDetail);
		model.addAttribute("allStudentDetail", allStudentDetail);*/
		
		List<PhotoInFooter> listOfSelectedStudent=footerPhotoService.listOfSelectedStudent();
		model.addAttribute("allStudentDetail", listOfSelectedStudent);
		
		model.addAttribute("subjectList", subjects);
		return "welcome";
	}

	@RequestMapping(value = "/aboutUs", method = RequestMethod.GET)
	public String aboutUs(Map<String, Object> map, Model model) {
		List<Subject> subjects = null;
		subjects = questionAnswerService.getAllSubjectName();
		List<PhotoInFooter> listOfSelectedStudent=footerPhotoService.listOfSelectedStudent();
		model.addAttribute("allStudentDetail", listOfSelectedStudent);
		model.addAttribute("subjectList", subjects);
		return "aboutUs";
		
	}

	@RequestMapping(value = "/features", method = RequestMethod.GET)
	public String features(Map<String, Object> map, Model model) {
		List<Subject> subjects = null;
		subjects = questionAnswerService.getAllSubjectName();
		List<PhotoInFooter> listOfSelectedStudent=footerPhotoService.listOfSelectedStudent();
		model.addAttribute("allStudentDetail", listOfSelectedStudent);
		model.addAttribute("subjectList", subjects);
		return "features";
	}

	@RequestMapping(value = "/termAndCondition", method = RequestMethod.GET)
	public String termAndCondition(Map<String, Object> map, Model model) {
		List<Subject> subjects = null;
		subjects = questionAnswerService.getAllSubjectName();
		List<PhotoInFooter> listOfSelectedStudent=footerPhotoService.listOfSelectedStudent();
		model.addAttribute("allStudentDetail", listOfSelectedStudent);
		model.addAttribute("subjectList", subjects);
		return "termAndCondition";
	}

	@RequestMapping(value = "/placementCell", method = RequestMethod.GET)
	public String placementCell(Map<String, Object> map, Model model) {
		List<Subject> subjects = null;
		subjects = questionAnswerService.getAllSubjectName();
		List<PhotoInFooter> listOfSelectedStudent=footerPhotoService.listOfSelectedStudent();
		model.addAttribute("allStudentDetail", listOfSelectedStudent);
		model.addAttribute("subjectList", subjects);
		return "placementCell";
	}

	@RequestMapping(value = "/course", method = RequestMethod.GET)
	public String course(Map<String, Object> map, Model model) {
		List<Subject> subjects = null;
		subjects = questionAnswerService.getAllSubjectName();
		List<PhotoInFooter> listOfSelectedStudent=footerPhotoService.listOfSelectedStudent();
		model.addAttribute("allStudentDetail", listOfSelectedStudent);
		model.addAttribute("subjectList", subjects);
		return "course";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Map<String, Object> map, Model model, @RequestParam(required = false) String invalid) {
		map.put("Registration", new Registration());
		model.addAttribute("invalid", invalid);
		List<Subject> subjects = null;
		subjects = questionAnswerService.getAllSubjectName();
		List<PhotoInFooter> listOfSelectedStudent=footerPhotoService.listOfSelectedStudent();
		model.addAttribute("allStudentDetail", listOfSelectedStudent);
		model.addAttribute("subjectList", subjects);
		return "login";
	}

	@RequestMapping(value = "/stuSignIn", method = RequestMethod.POST)
	public String signInAction(@ModelAttribute("Regestration") Registration registration, BindingResult result,
			ModelMap model, Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		registration = stuRegService.stuSignIn(registration);
		
		if (registration == null) {
			model.addAttribute("invalid", "Invalid user name and password");
			return "redirect:/login.do";
		} else {
			HttpSession session = request.getSession();
			session.setAttribute("registration", registration);
			return "redirect:/welcome.do";
		}
	}
/*start end blog*/
	@RequestMapping(value = "/blog", method = RequestMethod.GET)
	 public String blog(Map<String, Object> map, Model model) {
	  List<Subject> subjects = null;
	  subjects = questionAnswerService.getAllSubjectName();
	  List<PhotoInFooter> listOfSelectedStudent=footerPhotoService.listOfSelectedStudent();
		model.addAttribute("allStudentDetail", listOfSelectedStudent);
	  model.addAttribute("subjectList", subjects);
	  return "blog";
	 }
	
	@RequestMapping("/logout")
	public String showLogout(Map<String, Object> map, Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.invalidate();
		return "redirect:/login.do";
	}

	@RequestMapping(value = "/menu")
	public String menu(Map<String, Object> map, Model model) {
		System.out.println("+++++++++++++++++inside menu jsp+++++++++++++++++++++");
		return "menu";
	}

}
