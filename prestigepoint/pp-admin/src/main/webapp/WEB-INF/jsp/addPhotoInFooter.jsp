<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:eval
	expression="@propertyConfigurer.getProperty('pp.jspImagePath')"
	var="imgPath" />
<html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- <script type="text/javascript" src="js/page-js/registration.js"></script> -->
<script type="text/javascript">

function enableStudentStatus(studentDetail)
{
	alert("hi");
	var studentId=studentDetail.id;
	var studentValue=studentDetail.value;
	var data1;
	var b=document.getElementById(studentId).checked;
	
	if(b){		
		 data1={'studentId': studentValue,'checkedValue':"checked" };		
    }
	else{
	 	 data1 ={'studentId': studentValue,'checkedValue':"unchecked" };
					
	}
		
 	$.ajax({
		url : 'changeStudentStatus.do',
		data : data1,
		 contentType : "application/json; charset=utf-8", 
		success : function(response) {
			
		},
		error : function(error) {

		}
	}); 
		
	}

</script>
<script type="text/javascript">
var iBytesUploaded = 0;
var iBytesTotal = 0;
var iPreviousBytesLoaded = 0;
var iMaxFilesize = 1048576; // 1MB
var oTimer = 0;
var sResultFileSize = '';

function bytesToSize(bytes) {
	
    var sizes = ['Bytes', 'KB', 'MB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};


function fileSelected() {
	
	    // hide different warnings
	/*      document.getElementById('upload_response').style.display = 'none';
	    document.getElementById('error').style.display = 'none';
	    document.getElementById('error2').style.display = 'none';
	    document.getElementById('abort').style.display = 'none';
	    document.getElementById('warnsize').style.display = 'none';
   */
	    // get selected file element
	    var oFile = document.getElementById('image_file').files[0];

	    // filter for image files
	    var rFilter = /^(image\/bmp|image\/gif|image\/jpeg|image\/png|image\/tiff)$/i;
	    if (! rFilter.test(oFile.type)) {
	        document.getElementById('error').style.display = 'block';
	        return;
	    }

	    // little test for filesize
	     if (oFile.size > iMaxFilesize) {
	        document.getElementById('warnsize').style.display = 'block';
	        return;
	    } 

	    // get preview element
	    var oImage = document.getElementById('preview');

	    // prepare HTML5 FileReader
	    var oReader = new FileReader();
	        oReader.onload = function(e){

	        // e.target.result contains the DataURL which we will use as a source of the image
	        oImage.src = e.target.result;

	        oImage.onload = function () { // binding onload event

	            // we are going to display some custom image information here
	            sResultFileSize = bytesToSize(oFile.size);
	            document.getElementById('fileinfo').style.display = 'block';
	            document.getElementById('filename').innerHTML = 'Name: ' + oFile.name;
	            document.getElementById('filesize').innerHTML = 'Size: ' + sResultFileSize;
	            document.getElementById('filetype').innerHTML = 'Type: ' + oFile.type;
	            document.getElementById('filedim').innerHTML = 'Dimension: ' + oImage.naturalWidth + ' x ' + oImage.naturalHeight;
	         };
	    };

	    // read selected file as DataURL
	   oReader.readAsDataURL(oFile);
	}
</script>
<script type="text/javascript">
	function imgvalue() {
		
		var src = $("#preview").attr("src");
		
		$('#imagePath').attr("value", src);
	}
	$(document)
			.ready(
					function() {
						if ("${PhotoInFooter.studentId}" != "") {
							document.getElementById("preview").src = "${imgPath}${PhotoInFooter.studentId}.png";
						} else {
							document.getElementById("preview").src = "${imgPath}default.jpg";
						}
					});
</script>
<title>Add Courses</title>
</head>
<body>
  <div class="container clearfix">
    <div class="conact-form">
      <h3 style="color: red;">${message}</h3>
      <form:form method="POST" action="addFooterPhotoAction.do" modelAttribute="PhotoInFooter" autocomplete="off">
       <h3 style="color: #873d80;">Add Placed Student Detail For Dynamic Photo In Footer  </h3>
      
     			
        <table width="100%" border="0">
         <tr><td></td></tr> <tr><td></td></tr> <tr><td></td></tr>
          <tr>
            <td><div class="form-control">
                <form:errors path=""  class="label error-label"></form:errors>
              <label>
                  <p>
                    Student Name<small class="required"></small>
                  </p> <form:input path="studentName" class="inputControl1" placeholder="Student Name" required="autofocus"
                    maxlength="50"/> <form:hidden path="studentId" /> </label>
                  
              </div>
            </td>
             <td><div class="form-control">
                <form:errors path=""  class="label error-label"></form:errors>
                <label>
                  <p>
                   Student Comment<small class="required"></small>
                  </p> <form:textarea   path="studentComment" class="inputControl1" placeholder="Comments" required="autofocus"
                    maxlength="300"  /> </label>
              </div>
            </td>
          </tr>
          <tr>
            <td><div class="form-control">
                <form:errors path=""  class="label error-label"></form:errors>
                <label>
                  <p>
                   Company Name<small class="required"></small>
                  </p> <form:input path="placedInCompany" class="inputControl1" placeholder="Company Name" required="autofocus"
                    maxlength="50" /> </label>
                                  
              </div>
              </td>
              <td><div class="form-control">
             	<h4>
					<b>Please browse student image</b>
				</h4><%-- src="${imgPath}${PhotoInFooter.studentId}.png" --%>
             	 <img id="preview" src="${imgPath}${PhotoInFooter.studentId}.png" style="width: 50px; height: 50px" /><%-- onerror="this.src='${imgPath}image.jpg'" --%>
             <!-- 	<div id="error">You should select valid image files only!</div>
				<div id="error2">An error occurred while uploading the file</div>
				<div id="abort">The upload has been canceled by the user or
					the browser dropped the connection</div>
				<div id="warnsize">Your file is very big. We can't accept it.
					Please select more small file</div>
				<div id="progress_info">
					<div id="upload_response"></div>
				</div> -->
             	<input type="file" name="image_file" id="image_file"
							onchange="fileSelected();" />
						
		<div id="fileinfo">
					<div id="filename"></div>
					<div id="filesize"></div>
					<div id="filetype"></div>
					<div id="filedim"></div>
				</div>
				</div>
				
            </td>
             
          </tr>
            <tr>
            <td colspan="2"><input type="submit" value="Submit"  onclick="imgvalue()" class="btn lg-btn" />
            </td>
          </tr>
        </table>
    	<form:hidden path="imgPath" id="imagePath" />
      <c:set var="count" value="0" scope="page" />
      <display:table name="photoInFooterList" pagesize="8" class="basic-table" uid="cat" requestURI="addFooterPhoto.do">
        <c:set var="count" value="${count+1}" scope="page" />
        <display:column title="S.NO" class="showHeading" style="width:1%;">
     ${count}
    </display:column>
        <display:column property="studentName" title="STUDENT NAME" />
        <display:column property="placedInCompany" title="COMPANY NAME" />
        <display:column property="studentComment" title="COMMENT" />
            <display:column title="IMAGES" class="showHeading" style="width:1%;">
     <img id="preview" src="${imgPath}${cat.studentId}.png" style="width: 50px; height: 50px" />
    </display:column>
        <display:column title="Status">
        <c:if test="${cat.isStatusActive==1}">
     <form:checkbox path="isStatusActive" checked="checked" value="${cat.studentId}" id="checkboxId${cat.studentId}" onClick="enableStudentStatus(this);"/>
      </c:if>
        <c:if test="${cat.isStatusActive==0}">
     <form:checkbox path="isStatusActive" value="${cat.studentId}" id="checkboxId${cat.studentId}" onClick="enableStudentStatus(this);"/>
      </c:if>
      
        </display:column>
        <display:column title="Edit">
          <a href="addFooterPhotoAction.do?studentId=${cat.studentId}">Edit</a>
        </display:column>
        <display:column title="Delete">
          <a href="deleteStudentInfo.do?studentId=${cat.studentId}"
            onclick="return confirm('Please confirm if you want to delete this course!');">Delete</a>
        </display:column>
      </display:table>
        </form:form>
    </div>
  </div>
  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
</body>
</html>