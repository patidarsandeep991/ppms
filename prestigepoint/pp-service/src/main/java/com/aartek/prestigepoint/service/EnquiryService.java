package com.aartek.prestigepoint.service;

import com.aartek.prestigepoint.model.Enquiry;

public interface EnquiryService {

	public void addEnquiryMessage(Enquiry enquiry);

}
