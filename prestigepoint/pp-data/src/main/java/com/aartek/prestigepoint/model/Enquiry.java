package com.aartek.prestigepoint.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "enquiry")
public class Enquiry implements Serializable {
  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ENQUIRY_ID")
  private Integer enquiryId;

  @Column(name = "NAME")
  private String name;

  @Column(name = "MOBILE_NO")
  private String mobileNo;

  @Column(name = "EMAIL_ID")
  private String emailId;

  @Column(name = "COMMENT")
  private String comment;

  @Column(name = "IS_DELETED")
  private Integer isDeleted;

  @Column(name = "DATE")
  private String date;

  @Column(name = "SUBJECT")
  private String subject;

  /**
   * @return the enquiryId
   */
  public Integer getEnquiryId() {
    return enquiryId;
  }

  /**
   * @param enquiryId
   *          the enquiryId to set
   */
  public void setEnquiryId(Integer enquiryId) {
    this.enquiryId = enquiryId;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the mobileNo
   */
  public String getMobileNo() {
    return mobileNo;
  }

  /**
   * @param mobileNo
   *          the mobileNo to set
   */
  public void setMobileNo(String mobileNo) {
    this.mobileNo = mobileNo;
  }

  /**
   * @return the emailId
   */
  public String getEmailId() {
    return emailId;
  }

  /**
   * @param emailId
   *          the emailId to set
   */
  public void setEmailId(String emailId) {
    this.emailId = emailId;
  }

  /**
   * @return the comment
   */
  public String getComment() {
    return comment;
  }

  /**
   * @param comment
   *          the comment to set
   */
  public void setComment(String comment) {
    this.comment = comment;
  }

  /**
   * @return the isDeleted
   */
  public Integer getIsDeleted() {
    return isDeleted;
  }

  /**
   * @param isDeleted
   *          the isDeleted to set
   */
  public void setIsDeleted(Integer isDeleted) {
    this.isDeleted = isDeleted;
  }

  /**
   * @return the date
   */
  public String getDate() {
    return date;
  }

  /**
   * @param date
   *          the date to set
   */
  public void setDate(String date) {
    this.date = date;
  }

  /**
   * @return the subject
   */
  public String getSubject() {
    return subject;
  }

  /**
   * @param subject
   *          the subject to set
   */
  public void setSubject(String subject) {
    this.subject = subject;
  }

}
