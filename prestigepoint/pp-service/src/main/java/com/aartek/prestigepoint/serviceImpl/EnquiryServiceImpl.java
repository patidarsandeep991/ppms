package com.aartek.prestigepoint.serviceImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aartek.prestigepoint.model.Enquiry;
import com.aartek.prestigepoint.repository.EnquiryRepository;
import com.aartek.prestigepoint.service.EnquiryService;
import com.aartek.prestigepoint.util.IConstant;

@Service
public class EnquiryServiceImpl implements EnquiryService {
  @Autowired
  private EnquiryRepository enquiryRepository;

  public void addEnquiryMessage(Enquiry enquiry) {
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    enquiry.setIsDeleted(IConstant.IS_DELETED);
    enquiry.setDate(dateFormat.format(date));
    enquiryRepository.addEnquiryMessage(enquiry);
  }
}
