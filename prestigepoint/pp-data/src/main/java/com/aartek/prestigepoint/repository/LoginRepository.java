package com.aartek.prestigepoint.repository;

import java.util.List;

public interface LoginRepository {

  public List<Object> adminSignIn(String emailId, String password);

}
