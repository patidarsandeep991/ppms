package com.aartek.prestigepoint.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aartek.prestigepoint.model.Batch;
import com.aartek.prestigepoint.model.Emi;
import com.aartek.prestigepoint.model.Registration;
import com.aartek.prestigepoint.repository.EmiRepository;
import com.aartek.prestigepoint.service.EmiService;
import com.aartek.prestigepoint.util.DateFormat;
import com.aartek.prestigepoint.util.IConstant;
import com.aartek.prestigepoint.util.SendMail;

@Service
public class EmiServiceImpl implements EmiService {
  @Autowired
  private EmiRepository emiRepository;
  
  

  public List<Emi> getFeesDetails(Integer registrationId) {
    List<Emi> emis = new ArrayList<Emi>();
    emis = emiRepository.getFeesDetails(registrationId);
    return emis;
  }

  public boolean addEmiInfo(Emi emi) {
    boolean status = false;
    if (emi != null) {
    	System.out.println("we are in service");
      emi.setIsDeleted(IConstant.IS_DELETED);
      emi.setDate(DateFormat.getYYYYMMDDDate(emi.getDate()));
      String emiDate= emi.getDate();
      Integer amount= emi.getAmount();
      System.out.println("both get== "+emiDate+ ""+amount);
      Registration registration= emi.getRegistration();
      Integer stuRegistrationId   = registration.getRegistrationId();
      status = emiRepository.addEmiInfo(emi);
      List<Registration> studentDetails =emiRepository.getEmiDetails(stuRegistrationId);
     // String name= studentDetails.getFirstName();
      registration=studentDetails.get(0);
      SendMail.emiMail(registration.getEmailId(), registration.getSubmittedFee(),
    		  registration.getFirstName(), registration.getTotalFee(),emi.getAmount(),emi.getDate());
      
      return status;
    	    } else {	  
      return status;
    } 
    	
  }
  public boolean editEmiInfo(Emi emi) {
	   boolean status = false;
	   System.out.println("new method in service hhhhh");
	    if (emi.getEmiId() != null) {
	    	System.out.println("we are in service");
	      emi.setIsDeleted(IConstant.IS_DELETED);
	      emi.setDate(DateFormat.getYYYYMMDDDate(emi.getDate()));
	      String emiDate= emi.getDate();
	      Integer amount= emi.getAmount();
	      System.out.println("both get== "+emiDate+ ""+amount);
	      Registration registration= emi.getRegistration();
	      Integer stuRegistrationId   = registration.getRegistrationId();
	      status = emiRepository.editEmiInfo(emi);
	      List<Registration> studentDetails =emiRepository.getEmiDetails(stuRegistrationId);
	     // String name= studentDetails.getFirstName();
	      registration=studentDetails.get(0);
	      SendMail.editEmiMail(registration.getEmailId(), registration.getSubmittedFee(),
	    		  registration.getFirstName(), registration.getTotalFee(),emi.getAmount(),emi.getDate());
	      
	      return status;
	    	    } else {	  
	      return status;
	    } 
	
		
	}
 
 /* public List<Registration> getEmiDetails(Integer registrationId) {
	    List<Registration> list = new ArrayList<Registration>();
	    list = emiRepository.getEmiDetails(registrationId);
	    System.out.println("new method");
	    return list;
	  }*/
  

  public List<Registration> getRegistrationDetails(Integer registrationId) {
    List<Registration> list = new ArrayList<Registration>();
    list = emiRepository.getRegistrationDetails(registrationId);
    return list;
  }
  
  public Integer getRegistrationId(Integer emiId) {
	    List<Emi> emis=new ArrayList<Emi>();
	    Integer registrationId = null;
	    emis = emiRepository.getRegistrationId(emiId);
	    if(emis!=null)
	    {
	    	Emi emi=(Emi)emis.get(0);
	    	registrationId=emi.getRegistration().getRegistrationId();
	    }
	    return registrationId;
	  }

public void deleteEmiDetails(Integer emiId) {
	emiRepository.deleteEmiDetails(emiId); 
}

public Emi editEmi(Integer emiId) {
	 List<Object> list = new ArrayList<Object>();
	    Emi emi = null;
	    list = emiRepository.editEmi(emiId);
	    
	    for (Object object : list) {
	      emi = (Emi) object;
	    }
	    return emi;

}
public List<Emi> getAllEmiId() {
	List<Emi> list = new ArrayList<Emi>();
    list = emiRepository.getAllEmiId();
    return list;
	
}

public Emi editEimForSingleRecord(Integer emiId) {
	 List<Object> list = new ArrayList<Object>();
	 System.out.println("we are in edit serviceimpl edit");
	    Emi emi = null;
	    list = emiRepository.editEmi(emiId);
	    for (Object object : list) {
	      emi = (Emi) object;
	      
	    }
	    return emi;
}


  }
